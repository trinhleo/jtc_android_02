package com.janeto.jtc.bai13webapi;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static ApiService apiService;

    private Retrofit retrofit;

    private ApiService(String baseUrl) {
        initClient(baseUrl);
    }

    private void initClient(@NonNull String baseUrl) {
        if (TextUtils.isEmpty(baseUrl)) {
            return;
        }
        if (!baseUrl.endsWith("/")) {
            baseUrl = baseUrl + "/";
        }
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.build();
    }

    public static void init(@NonNull String baseUrl) {
        if (apiService == null) {
            apiService = new ApiService(baseUrl);
        }
    }

    public static ApiService getInstance() {
        return apiService;
    }

    public void getGithub(Callback<GitHubApi> callback) {

        if (retrofit != null) {
            Call<GitHubApi> getGithubApi = retrofit.create(ApiDemo.class).getGithub();
            getGithubApi.enqueue(callback);
        }
    }

//    public void getRepos(String owner, String repos, Callback<Repos> callback) {
//
//        if (retrofit != null) {
//            Call<Repos> register =
//                    retrofit.create(ApiDemo.class).getRepos("https://api.github.com/repos/"+owner+"/"+repos);
//            register.enqueue(callback);
//        }
//    }


}
