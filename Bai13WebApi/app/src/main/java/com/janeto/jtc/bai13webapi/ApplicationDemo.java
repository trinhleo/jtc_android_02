package com.janeto.jtc.bai13webapi;

import android.app.Application;

public class ApplicationDemo extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApiService.init("https://api.github.com/");
    }
}
