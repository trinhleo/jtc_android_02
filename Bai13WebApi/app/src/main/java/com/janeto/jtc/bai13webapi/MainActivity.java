package com.janeto.jtc.bai13webapi;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button getBtn;
    private TextView resultTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        getBtn = findViewById(R.id.buttonGet);
        getBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiUseLib();
            }
        });
        resultTxt = findViewById(R.id.resultTxt);
    }

    private void getGithub() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
//Create URL
                try {
                    URL githubEndpoint = new URL("https://api.github.com/");
                    // Create connection
                    HttpsURLConnection myConnection = (HttpsURLConnection) githubEndpoint.openConnection();

                    myConnection.setRequestProperty("User-Agent", "my-rest-app-v0.1");

                    myConnection.setRequestProperty("Accept", "application/vnd.github.v3+json");

                    myConnection.setRequestProperty("Contact-Me",
                            "hathibelagal@example.com");

                    if (myConnection.getResponseCode() == 200) { // Success
                        InputStream responseBody = myConnection.getInputStream();

                        InputStreamReader responseBodyReader =
                                new InputStreamReader(responseBody, "UTF-8");

                        JsonReader jsonReader = new JsonReader(responseBodyReader);
                        Log.d("result", responseBody.toString());
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String key = jsonReader.nextName();
//                            if (key.equals("notifications_url")) {

                            String value = jsonReader.nextString();

                            Log.d("result", value);

//                                break;
//                            } else {
//                                jsonReader.skipValue();
//                            }

                        }
                        jsonReader.close();
                        myConnection.disconnect();
                    } else {
// Error handling code goes here
                        Log.d("result", myConnection.getResponseCode() + "");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void callApiUseLib(){
        ApiService.getInstance().getGithub(new Callback<GitHubApi>() {
            @Override
            public void onResponse(Call<GitHubApi> call, Response<GitHubApi> response) {
                if(response.isSuccessful()){
                    GitHubApi body = response.body();
                    resultTxt.setText(body.getNotificationsUrl());
                } else {

                }

            }

            @Override
            public void onFailure(Call<GitHubApi> call, Throwable t) {

            }
        });
    }
}
