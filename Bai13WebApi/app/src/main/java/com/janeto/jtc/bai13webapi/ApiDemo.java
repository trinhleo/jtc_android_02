package com.janeto.jtc.bai13webapi;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiDemo {
    @GET("/")
    Call<GitHubApi> getGithub();
//    @GET
//    Call<Repos> getRepos(@Url String url);
}
