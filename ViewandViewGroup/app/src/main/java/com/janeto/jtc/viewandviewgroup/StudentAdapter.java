package com.janeto.jtc.viewandviewgroup;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class StudentAdapter extends ArrayAdapter<Student> {
    List<Student> listData;
    LayoutInflater inflater;
    Activity mContext;
    int layoutId;
    private interfacedemo itf;

    public StudentAdapter(@NonNull Activity context, int resource, @NonNull List<Student> objects, interfacedemo ift ) {
        super(context, resource, objects);
        this.mContext = context;
        this.layoutId = resource;
        this.listData = objects;
        this.itf = ift;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = mContext.getLayoutInflater();
        final Student item = listData.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(layoutId, parent, false);
            holder.setTxtName((TextView) convertView.findViewById(R.id.txtName));
            holder.setTxtPhone((TextView) convertView.findViewById(R.id.txtPhone));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.getTxtName().setText(item.getName());
        holder.getTxtPhone().setText(item.getPhone());
        holder.getTxtName().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itf.select(item.getName());
            }
        });

        return convertView;
    }

    private class ViewHolder{
        private TextView txtName;
        private TextView txtPhone;

        public TextView getTxtName() {
            return txtName;
        }

        public void setTxtName(TextView txtName) {
            this.txtName = txtName;
        }

        public TextView getTxtPhone() {
            return txtPhone;
        }

        public void setTxtPhone(TextView txtPhone) {
            this.txtPhone = txtPhone;
        }
    }
}
