package com.janeto.jtc.viewandviewgroup;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements interfacedemo {
    private ListView listView;
    private GridView gridView;
    private  StudentAdapter arrayAdapter;
    private List<Student> data;
    private Button addBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_demo);
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setBackground(new CustomDrawable());
//        initView();
    }

    private void initView() {
        listView = findViewById(R.id.listView1);
        gridView = findViewById(R.id.GridView1);
        addBTN = findViewById(R.id.addBtn);
        data = new ArrayList<>();

        listView.setBackgroundResource(R.drawable.ic_launcher_background);

        for(int i = 0; i<5; i++){
            data.add(new Student(i,"hs" + i + 1,"0123456789" +i));
        }
        arrayAdapter = new StudentAdapter(this, R.layout.student_list_item, data, this);
//        listView.setAdapter(arrayAdapter);
        gridView.setAdapter(arrayAdapter);
//        addBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                data.add(new Student(data.size(),"hoc sinh","01233214"));
//                arrayAdapter.notifyDataSetChanged();
//            }
//        });
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(MainActivity.this,data.get(i).getName(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }


    @Override
    public void select(String name) {
        Toast.makeText(MainActivity.this,name,Toast.LENGTH_SHORT).show();
    }
}
