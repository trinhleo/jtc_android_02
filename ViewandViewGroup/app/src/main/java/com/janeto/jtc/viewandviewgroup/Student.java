package com.janeto.jtc.viewandviewgroup;

public class Student {
    private int id;
    private String Name;
    private String Phone;

    public Student(int id, String name, String phone) {
        this.id = id;
        Name = name;
        Phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
