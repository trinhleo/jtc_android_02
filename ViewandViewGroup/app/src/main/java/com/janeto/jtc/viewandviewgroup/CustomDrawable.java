package com.janeto.jtc.viewandviewgroup;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class CustomDrawable extends Drawable {
    @Override
    public void draw(@NonNull Canvas canvas) {
        Paint paint = new Paint();
        Rect rect = getBounds();
        paint.setColor(Color.RED);
        canvas.drawLine(rect.left,rect.top +50,rect.right,rect.bottom, paint);
        canvas.drawCircle(rect.centerX(),rect.centerY(),50,paint);
        canvas.save();
    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return Color.TRANSPARENT;
    }
}
