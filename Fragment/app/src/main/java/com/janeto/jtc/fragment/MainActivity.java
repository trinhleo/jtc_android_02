package com.janeto.jtc.fragment;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.plus.internal.PlusCommonExtras;

public class MainActivity extends AppCompatActivity implements IBlankFragement {
    private Button button1, button2;
    private IMain iMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       initView();
       iMain.addMore();
    }

    private void initView(){
        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createFragment1();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createFragment2();
            }
        });
    }

    private void createFragment1() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        BlankFragment blankFragment = new BlankFragment();

        blankFragment.setListener(this);
        fragmentTransaction.replace(R.id.fragmentContainer,blankFragment);
        fragmentTransaction.commit();

    }

    private void createFragment2() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PlusOneFragment plusOneFragment = new PlusOneFragment();
        plusOneFragment.setmListener(new IPlusOneFragment() {
            @Override
            public void onClick() {

            }
        });
        fragmentTransaction.replace(R.id.fragmentContainer,plusOneFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }

    public void abc(){
       Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
       if(currentFragment instanceof BlankFragment){
           ( (BlankFragment) currentFragment).addMoreButton();
       } else if (currentFragment instanceof PlusOneFragment){
           ( (PlusOneFragment) currentFragment).addMore();
       }

    }


    @Override
    public void onClick() {

    }

    @Override
    public void setMainListener(IMain iMain) {
        this.iMain = iMain;
    }
}
