package com.janeto.jtc.animation;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageViewSun;
    private ImageView imageViewWheel;
    private ImageView imageBoy;
    private Button buttonStart;
    private Button buttonStop;
    private Button buttonResume;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        imageViewSun = findViewById(R.id.imageSun);
        imageViewWheel = findViewById(R.id.imageWheel);
        buttonStart = findViewById(R.id.start);
        buttonStop = findViewById(R.id.stop);
        buttonResume = findViewById(R.id.resumBtn);
        imageBoy = findViewById(R.id.boy);
        setAnimation();

    }

    private void setAnimation() {
        final AnimatorSet sunSet = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.sun_swing);

        final AnimatorSet wheelSet = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.wheel_spin);

        sunSet.setTarget(imageViewSun);

        wheelSet.setTarget(imageViewWheel);
        sunSet.start();
        wheelSet.start();
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wheelSet.playTogether(sunSet);
//                wheelSet.start();
//                sunSet.playTogether(wheelSet);
//                sunSet.start();
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//              sunSet.end();
//                wheelSet.end();
//                sunSet.cancel();
//                wheelSet.cancel();
                sunSet.pause();


            }
        });
        buttonResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sunSet.resume();
            }
        });


//
//        imageBoy.setBackgroundResource(R.drawable.animation_list_demo);
//        AnimationDrawable peopleAnimation = (AnimationDrawable) imageBoy.getBackground();
//        peopleAnimation.start();

    }

}
