package com.janeto.jtc.luutrudulieu;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;



/**
 * Created by janeto on 11/29/17.
 */

public class SharedPreferencesManager {
    private static final String PREF_FIRST_TIME_SETUP = BuildConfig.APPLICATION_ID + ".pref_first_time_setup";
    private static final String PREF_FIRST_TIME_SETUP2 = BuildConfig.APPLICATION_ID + ".pref_first_time_setup2";


    private static SharedPreferences sPreferences;

    private SharedPreferencesManager() {
    }

    public static void init(Context context) {
        if (sPreferences == null) {
            sPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    public static void setFirstTimeSetup(boolean isFirstTime) {
        SharedPreferences.Editor editor = sPreferences.edit();
        editor.putBoolean(PREF_FIRST_TIME_SETUP, isFirstTime);
        editor.apply();
    }

    public static boolean isFirstTimeSetup() {
        return sPreferences.getBoolean(PREF_FIRST_TIME_SETUP, true);
    }

    public static void setFirstTimeSetup2(boolean isFirstTime) {
        SharedPreferences.Editor editor = sPreferences.edit();
        editor.putBoolean(PREF_FIRST_TIME_SETUP, isFirstTime);
        editor.apply();
    }

    public static boolean isFirstTimeSetup2() {
        return sPreferences.getBoolean(PREF_FIRST_TIME_SETUP, true);
    }


}
