package com.janeto.jtc.luutrudulieu;

import android.app.Application;

import com.janeto.jtc.luutrudulieu.db.DbService;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferencesManager.init(this);
        DbService.init(this);
    }
}
