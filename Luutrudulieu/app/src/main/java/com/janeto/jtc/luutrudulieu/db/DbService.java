package com.janeto.jtc.luutrudulieu.db;

import android.content.Context;
import android.database.Cursor;

public class DbService {
    private static DbService dbService;
    public static void init(Context context){
        if (dbService == null) {
            dbService = new DbService(context);
        }

    }

    private FeedReaderDao feedReaderDao;

    public DbService(Context context){
        feedReaderDao = new FeedReaderDao(context);
    }

    public static DbService getInstance() {
        return dbService;
    }

    public Feed getFeedByTitle(String title){

      Cursor cursor = feedReaderDao.readFeedEntryByTile(title);
        // get readable database as we are not inserting anything

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        Feed feed = new Feed(
                cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE)),
                cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE)));

        // close the db connection
        cursor.close();

        return feed;
    }

    public void createFeed(Feed feed) {
        feedReaderDao.createFeedReader(feed.getTitle(),feed.getSubtitle());
    }

}
