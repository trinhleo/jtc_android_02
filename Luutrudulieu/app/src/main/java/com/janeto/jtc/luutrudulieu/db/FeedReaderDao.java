package com.janeto.jtc.luutrudulieu.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class FeedReaderDao {
    private FeedReaderDbHelper mDbHelper;

    public FeedReaderDao(Context context) {
        this.mDbHelper = new FeedReaderDbHelper(context);

    }

    public long createFeedReader(String title, String subtitle) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //chuan bi noi dung
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, title);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE, subtitle);

//      Insert the new row, returning the primary key value of the new row

        return db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);

    }

    public Cursor readFeedEntryByTile(String title) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Define a projection that specifies which columns from the database // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE};
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " = ?";
        String[] selectionArgs = {title};
        // How you want the results sorted in the resulting Cursor
        String sortOrder = FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE + " DESC";

        return db.query(FeedReaderContract.FeedEntry.TABLE_NAME, // The table to query
                projection, selection, selectionArgs, null,
                null, sortOrder);

    }

    public boolean deleteFeedEntry(long id) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
// Define 'where' part of query.
        String selection = FeedReaderContract.FeedEntry._ID + "=" + id;
// Specify arguments in placeholder order.
        String[] selectionArgs = {"MyTitle"};
// Issue SQL statement.
        return db.delete(FeedReaderContract.FeedEntry.TABLE_NAME, selection, null) > 0;
    }

    public int updateFeedEntry(long id, String newTitle, String newSubtile){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
// New value for one column
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, newTitle);

// Which row to update, based on the title
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " LIKE ?";
        String[] selectionArgs = { "MyOldTitle" };

        return db.update( FeedReaderContract.FeedEntry.TABLE_NAME, values,
                selection,
                selectionArgs);
    }

    public void closeDb() {
        mDbHelper.close();
    }
}
