package com.janeto.jtc.luutrudulieu;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.janeto.jtc.luutrudulieu.db.DbService;
import com.janeto.jtc.luutrudulieu.db.Feed;
import com.janeto.jtc.luutrudulieu.db.FeedReaderDao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_READ_EXTERNAL = 123;
    private static final int REQUEST_WRITE_EXTERNAL = 124;
    private Button writeBtn, readBtn;
    private EditText nameEdt;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        readExternalFile();
    }

    private void initView(){
        writeBtn = findViewById(R.id.button);
        readBtn = findViewById(R.id.button2);
        nameEdt = findViewById(R.id.editText);
        textView = findViewById(R.id.textView);
        writeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeFile(nameEdt.getText().toString());
            }
        });

        readBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(readFile());
            }
        });
    }

    private void writeFile(String fileContents){

//        FileOutputStream outputStream;
//        try {
//            outputStream = openFileOutput("abc.txt", Context.MODE_PRIVATE);
//            outputStream.write(fileContents.getBytes());
//            outputStream.close();
//        } catch (Exception e) { e.printStackTrace();
//        }
        DbService.getInstance().createFeed(new Feed("abc", fileContents));
    }

    private String readFile() {
        //Read file in Internal Storage
//        FileInputStream fis;
//        StringBuilder content = new StringBuilder();
//        try {
//            fis = openFileInput("abc.txt");
//            byte[] input = new byte[fis.available()];
//            while (fis.read(input) != -1) {
//                content.append(new String(input));
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
       String title =  DbService.getInstance().getFeedByTitle("abc").getSubtitle();
        return title;
    }

    private void readExternalFile(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
               doReadExternalFile();
            } else {
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL);
            }
        } else {
            doReadExternalFile();
        }

    }

    private void doReadExternalFile(){

    }
    private void writeExternalFile(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                doWriteExternalFile();
            } else {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,}, REQUEST_WRITE_EXTERNAL);
            }
        } else {
            doWriteExternalFile();
        }
    }

    private void doWriteExternalFile(){

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_READ_EXTERNAL) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doReadExternalFile();
            }
        } else if(requestCode == REQUEST_WRITE_EXTERNAL) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doWriteExternalFile();
            }
        }
    }
}
