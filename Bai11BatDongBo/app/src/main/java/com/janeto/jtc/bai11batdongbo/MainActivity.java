package com.janeto.jtc.bai11batdongbo;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView helloTxt;
    private TextView progressTxt;
    private Button dlBtn;
    private Button playBtn;
    private Button stopBtn;
    private Button showWeatherBtn;
    private EditText locationEdt;
    private WeatherService weatherService;
    private TextView weatherTv;
    private boolean binded = false;
    private ServiceConnection weatherServiceConnection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            WeatherService.LocalWeatherBinder binder = (WeatherService.LocalWeatherBinder) iBinder;
            weatherService = binder.getService();
            binded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            binded = false;
        }
    };

    private Intent serviceIntent;

    private ResponseReceiver receiver = new ResponseReceiver();
    private ProgressBar progressBar;
    private Button startButton;
    private Button stopButton;
    private TextView percelText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        threadDemo();
        handlerDemo();

        dlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asyncTaskDemo();
            }
        });
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSong();
            }
        });
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopSong();
            }
        });

        showWeatherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWeather();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        bindWeatherService();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Đăng ký bộ thu sóng với Activity.
        registerReceiver(receiver, new IntentFilter(
                SimpleIntentService.ACTION_1));
    }



    private void showWeather() {
        if (binded) {
            String location = locationEdt.getText().toString();
            String weather = weatherService.getWeatherToday(location);
            weatherTv.setText(weather);
        } else {
            bindWeatherService();
        }

    }

    private void bindWeatherService() {
        Intent intent = new Intent(this, WeatherService.class);
// Gọi method bindService(..) để ràng buộc dịch vụ với giao diện.
        this.bindService(intent, weatherServiceConnection,
                Context.BIND_AUTO_CREATE);
    }


    private void initView() {
        helloTxt = findViewById(R.id.hellTxt);
        progressTxt = findViewById(R.id.progressTxt);
        dlBtn = findViewById(R.id.button);
        playBtn = findViewById(R.id.buttonPlaySong);
        stopBtn = findViewById(R.id.buttonStop);
        showWeatherBtn = findViewById(R.id.buttonShowWeather);
        weatherTv = findViewById(R.id.tvWeather);
        locationEdt = findViewById(R.id.editTextLocation);
        this.startButton = this.findViewById(R.id.intentStartBtn);
        this.stopButton = this.findViewById(R.id.intentStopBtn);
        this.percelText = this.findViewById(R.id.percentText);
        this.progressBar = this.findViewById(R.id.progressBar);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startButton.setEnabled(false);
                serviceIntent = new Intent(MainActivity.this, SimpleIntentService.class);
                MainActivity.this.startService(serviceIntent);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(serviceIntent!= null) {
                    stopService(serviceIntent);
                }
            }
        });
    }

    // Khi Activity ngừng hoạt động.
    @Override
    protected void onStop() {
        super.onStop();
        if (binded) {
// Hủy ràng buộc kết nối với dịch vụ.
            this.unbindService(weatherServiceConnection);
            binded = false;
        }

        // Hủy đăng ký bộ thu sóng với Activity.
        unregisterReceiver(receiver);
    }


    private void playSong() {
        // Tạo ra một đối tượng Intent cho một dịch vụ (PlaySongService).
        Intent myIntent = new Intent(MainActivity.this,
                PlaySongService.class);
        // Gọi phương thức startService (Truyền vào đối tượng Intent)
        this.startService(myIntent);
    }

    // Method này được gọi khi người dùng Click vào nút Stop.
    private void stopSong() {
        // Tạo ra một đối tượng Intent.
        Intent myIntent = new Intent(MainActivity.this,
                PlaySongService.class);
        this.stopService(myIntent);
    }

    private void threadDemo() {
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(5000);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            helloTxt.setText("def");
                            Log.d("txt", "def");
                        }
                    });
//                    ((TextView) findViewById(R.id.hello)).setText("abc");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void handlerDemo() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                helloTxt.setText("abc");
                Log.d("txt", "abc");
            }
        }, 5000);

    }

    private void asyncTaskDemo() {
        new ProgressTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class ProgressTask extends AsyncTask<Void, Integer, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            int total = 0;
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(100);
                    publishProgress(total);
                    total++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return total;
        }


        @Override
        protected void onPostExecute(Integer aVoid) {
            progressTxt.setText("Completed");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressTxt.setText(values[0] + "");
        }


    }

    // Broadcast component
// Class mô phỏng một bộ thu sóng
// (Thu tín hiệu gửi từ Service).
    public class ResponseReceiver extends BroadcastReceiver {
        // on broadcast received
        @Override
        public void onReceive(Context context, Intent intent) {
// Kiểm tra nhiệm vụ của Intent gửi đến.
            if (intent.getAction().equals(SimpleIntentService.ACTION_1)) {
                int value = intent.getIntExtra("percel", -1);
                new ShowProgressBarTask().execute(value);
            }
        }
    }

    // Class làm nhiệm vụ hiển thị giá trị cho ProgressBar.
    class ShowProgressBarTask extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... args) {
            return args[0];
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progressBar.setProgress(result);
            percelText.setText(result + " % Loaded");
            if (result == 100) {
                percelText.setText("Completed");
                startButton.setEnabled(true);
            }
        }
    }
}
