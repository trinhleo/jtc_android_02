package com.janeto.jtc.android02bai1;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView mHelloTv;
    private TextView mGotoButon;


    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "on resume", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        handleTextView();
        Toast.makeText(this, "on start", Toast.LENGTH_SHORT).show();
    }



    private void initView() {
        mHelloTv = findViewById(R.id.textViewHello);
        mGotoButon = findViewById(R.id.goto2Button);

        mGotoButon.setOnClickListener(this);
    }

    private void handleTextView() {

        mHelloTv.setText("khong thich hello");
    }

    private void gotoActivity2() {
        Intent intent = new Intent(this,Main2Activity.class);
        intent.putExtra(Constants.ACTIVITY_DATA,1);
//        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//        sendIntent.putExtra("sms_body", "trinh");
//                sendIntent.setType("vnd.android-dir/mms-sms");
//
//        startActivity(sendIntent);
        startActivityForResult(intent,Constants.DEMO_REQUEST_CODE);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.goto2Button) {
            gotoActivity2();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.DEMO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Toast.makeText(MainActivity.this, Objects.requireNonNull(data.getExtras()).get(Constants.ACTIVITY_DATA) + "",Toast.LENGTH_LONG).show();
        }
    }


}
