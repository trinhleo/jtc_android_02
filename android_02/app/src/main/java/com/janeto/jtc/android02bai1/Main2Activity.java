package com.janeto.jtc.android02bai1;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        if(getIntent().getExtras() !=null) {
            Toast.makeText(Main2Activity.this, getIntent().getExtras().getInt(Constants.ACTIVITY_DATA) + "",Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onBackPressed() {

        getIntent().putExtra(Constants.ACTIVITY_DATA, "hello");
        setResult(Activity.RESULT_OK, getIntent());
        finish();
    }
}
